import { getFloorAt, getFloor } from '../../../src/utils/geom.js';

const getStructures = async app => app.bus.get('venueData/getVenueData').then(vd => vd.structures);

async function locationToEndpoint (app, location) {
  if (location.poiId)
    return app.bus.get('wayfinder/getNavigationEndpoint', { ep: location.poiId })
  const { lat, lng, ord, floorId, title = '' } = location;
  const structures = await getStructures();
  const floor = ord !== undefined ? getFloorAt(structures, lat, lng, ord) : getFloor(structures, floorId);
  if (ord !== undefined)
    return { lat, lng, ordinal: ord, floorId: floor ? floor.id : null, title }
  if (!floor)
    throw Error('Call to locationToEndpoint with no ordinal and no floorId (or an invalid one): ' + floorId)
  return { lat, lng, floorId, ordinal: floor.ordinal, title }
}

export { getStructures, locationToEndpoint };
