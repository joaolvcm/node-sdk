import fetch from 'node-fetch';
import pkg from '../package.json.js';
import { create } from '../src/controller.js';
import create$1 from '../src/utils/observable.js';
import prepareSDKConfig from './prepareSDKConfig.js';

const version = pkg.version;

if (!globalThis.fetch) {
  globalThis.fetch = fetch;
}

// this iterates through available commands and makes them member functions of map
function addCommands (map, commands) {
  commands.forEach(sig => {
    map[sig.command] = function () {
      const cob = { command: sig.command };
      for (let i = 0; i < arguments.length; i++)
        cob[sig.args[i].name] = arguments[i];
      return map(cob)
    };
  });
}

async function newMap (sdkConfig) {
  return new Promise((resolve, reject) => {
    sdkConfig.headless = true ;

    const config = prepareSDKConfig(sdkConfig);

    create(config)
      .then(app => {
        const map = (payload, arg2) => {
          if (typeof payload === 'string')
            payload = { ...arg2, command: payload };
          return app.bus.get('clientAPI/execute', payload)
        };

        create$1(map); // make map handle events pub/sub
        app.eventListener.detachAll();
        app.eventListener.observe(function () { map.fire.apply(map, arguments); }); // pass it on...

        map.on('ready', (eName, data) => {
          const { commands/*, customTypes */ } = data.commandJSON;
          addCommands(map, commands);
          resolve(map);
        });
      });
  })
}

const Init = {
  getVersion: () => version,
  newMap: config => newMap(config),
  setLogging: flag => { }
};

export default Init;
