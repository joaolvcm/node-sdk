import * as R from 'ramda';

// https://stackoverflow.com/questions/22521982/check-if-point-inside-a-polygon
const pointInPolygon = (point, vs) => {
  const x = point[0];
  const y = point[1];

  let inside = false;
  for (let i = 0, j = vs.length - 1; i < vs.length; j = i++) {
    const xi = vs[i][0];
    const yi = vs[i][1];
    const xj = vs[j][0];
    const yj = vs[j][1];
    const intersect = ((yi > y) !== (yj > y)) &&
          (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
    if (intersect) {
      inside = !inside;
    }
  }

  return inside
};

/**
 * Pass in a buildings array and a lat,lng and this will return the building at that point
 * or undefined if no building exists at that point. This runs very quickly as we very often
 * can skip the slowest path (which runs pointInPolygon). Without preciseFlag we sometimes return
 * a building when the point falls outside it - but won't return a wrong building. In some cases this
 * is actually desired behavior anyway (such as building selector)
 * @param  {Array.Object} buildings
 * @param  {float} lat latitude of point to check
 * @param  {float} lng longitude of point to check
 * @param  {boolean} preciseFlag if not true we take a shortcut when a single building falls within bounding box (very common case)
 * @returns {object} building that contains the point passed, or undefined if none
 */
function getStructureAtPoint (buildings, lat, lng, preciseFlag) {
  if (!R.length(buildings)) return null

  buildings = buildings.filter(b => b.shouldDisplay == null || b.shouldDisplay === true);

  const pointInPolygonCurried = R.curry(pointInPolygon)([lat, lng]);

  const buildingsWithinBoundingBox = buildings
    .filter(R.compose(pointInPolygonCurried, getBoundsCoords));

  if (buildingsWithinBoundingBox.length === 0) return null
  if (buildingsWithinBoundingBox.length === 1 && !preciseFlag)
    return R.head(buildingsWithinBoundingBox)

  const buildingsWithinBoundsPolygon = buildingsWithinBoundingBox
    .filter(R.compose(pointInPolygonCurried, R.prop('boundsPolygon')));

  if (buildingsWithinBoundsPolygon.length === 1)
    return R.head(buildingsWithinBoundsPolygon)

  if (buildingsWithinBoundsPolygon.length)
    return findBuildingWithMaxArea(buildingsWithinBoundingBox)
  return null
}

const getBoundsCoords = building => {
  const { n, s, e, w } = building.bounds;
  return [[n, e], [n, w], [s, w], [s, e]]
};

const calcBuildingArea = currentBuilding => {
  if (!currentBuilding.bounds) return 0
  const { n, s, e, w } = currentBuilding.bounds;
  return Math.abs((n - s) * (e - w))
};

const findBuildingWithMaxArea = R.reduce(R.maxBy(calcBuildingArea), {});

/**
 * given a building and ord, return the floor (or undefined if doesn't exist)
 */
const ordToFloor = (building, ord) =>
  Object.values(building.levels).reduce((matchedFloor, floor) =>
    floor.ordinal === ord ? floor : matchedFloor, undefined);

/**
 * Return the floor based on its ID (pass in buildings array and floorId)
 */
const getFloor = (buildings, selectedLevelId) =>
  buildings.reduce((fmatch, building) =>
    Object.values(building.levels).reduce((fmatch, floor) =>
      floor.id === selectedLevelId ? floor : fmatch, null) || fmatch, undefined);

function getBuildingAndFloorAtPoint (buildings, lat, lng, ord, preciseFlag) {
  const building = getStructureAtPoint(buildings, lat, lng, preciseFlag);
  const floor = building
    ? ordToFloor(building, ord)
    : null;
  return { building, floor }
}

const getFloorAt = (buildings, lat, lng, ord, preciseFlag) => getBuildingAndFloorAtPoint(buildings, lat, lng, ord, preciseFlag).floor;

/**
 * Calculate the points for a bezier cubic curve
 *
 * @param {number} fromX - Starting point x
 * @param {number} fromY - Starting point y
 * @param {number} cpX - Control point x
 * @param {number} cpY - Control point y
 * @param {number} cpX2 - Second Control point x
 * @param {number} cpY2 - Second Control point y
 * @param {number} toX - Destination point x
 * @param {number} toY - Destination point y
 * @return {Object[]} Array of points of the curve
 */
function bezierCurveTo (fromX, fromY, cpX, cpY, cpX2, cpY2, toX, toY) {
  const n = 20; // controls smoothness of line
  let dt = 0;
  let dt2 = 0;
  let dt3 = 0;
  let t2 = 0;
  let t3 = 0;

  const path = [{ x: fromX, y: fromY }];

  for (let i = 1, j = 0; i <= n; ++i) {
    j = i / n;

    dt = (1 - j);
    dt2 = dt * dt;
    dt3 = dt2 * dt;

    t2 = j * j;
    t3 = t2 * j;

    path.push({
      x: (dt3 * fromX) + (3 * dt2 * j * cpX) + (3 * dt * t2 * cpX2) + (t3 * toX),
      y: (dt3 * fromY) + (3 * dt2 * j * cpY) + (3 * dt * t2 * cpY2) + (t3 * toY)
    });
  }

  return path
}

export { bezierCurveTo, getBuildingAndFloorAtPoint, getFloor, getFloorAt, getStructureAtPoint, ordToFloor, pointInPolygon };
