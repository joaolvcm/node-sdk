import { path, identity, prop } from 'ramda';

function findBoundsOfWaypoints (waypoints) {
  const latitudes = waypoints.map(path(['position', 'lat'])).filter(identity);
  const longitudes = waypoints.map(path(['position', 'lng'])).filter(identity);
  return findBounds(latitudes, longitudes)
}

function findBoundsOfCoordinates (coordinates) {
  const latitudes = coordinates.map(prop(0)).filter(identity);
  const longitudes = coordinates.map(prop(1)).filter(identity);
  return findBounds(latitudes, longitudes)
}

function findBounds (latitudes, longitudes) {
  const n = Math.max(...latitudes);
  const s = Math.min(...latitudes);
  const e = Math.max(...longitudes);
  const w = Math.min(...longitudes);
  return { n, s, e, w }
}

export { findBoundsOfCoordinates, findBoundsOfWaypoints };
