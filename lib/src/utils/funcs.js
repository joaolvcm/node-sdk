import Zousan from 'zousan';

// Ensures only a single asynchronous function wrapped in this function runs at a time.
let lastFn = null;
const singleFile = fn => function () {
  if (lastFn)
    lastFn = lastFn.then(() => fn.apply(null, arguments));
  else
    lastFn = fn.apply(null, arguments);

  return lastFn
};

// call a function after a delay with an optional value specified. Can be used
// as HOF or directly as a promise. Cool eh? This allows you to use it in a
// function pipeline, or directly in an await delay, etc.
//
// Usage:
//
//  await delay(500) // waits for 500ms - returns undefined
//  await delay(500)(123) // waits for 500ms - returns 123
//
//  f1().then(delay(800)).then(f2) // delay 800ms, pass result on to f2
//  R.pipe(f1, delay(800), f2) // same as above
const delay = ms => {
  const r = v => new Zousan(y => setTimeout(y, ms, v));
  const z2 = new Zousan(y => setTimeout(y, ms));
  r.then = z2.then.bind(z2);
  return r
};

/**
 * returns a copy of an object with any (top-level) properties that do not
 * return truthy from the specified function stripped. So it works just like
 * the Array.filter, but for objects.
 * @param  {function} fn A filter predicate - will be passed (key,object)
 * @param  {object} ob The object to filter (top level properties only)
 * @returns (object) The newly created object with filtered properties
 */
function filterOb (fn, ob) {
  const ret = { };
  Object.keys(ob)
    .forEach(key => {
      if (fn(key, ob[key]))
        ret[key] = ob[key];
    });
  return ret
}

export { delay, filterOb, singleFile };
