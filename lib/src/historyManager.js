const initHistoryManager = ({ bus }) => {
  const stepStack = [];
  bus.on('history/register', ({ viewId, event, params = {} }) => {
    stepStack.push({ viewId, event, params });
  });
  bus.on('history/stepBack', () => {
    const layersToHide = [];
    const current = stepStack.pop();

    if (!current) return

    layersToHide.push(current.viewId);
    for (let i = stepStack.length - 1; i >= 0; --i) {
      if (stepStack[i].event === current.event) {
        const same = stepStack.pop();
        layersToHide.push(same.viewId);
      } else {
        break
      }
    }
    bus.send('layers/hideMultiple', layersToHide);
    const { event, params } = stepStack.pop();
    bus.send(event, params);
  });
  bus.on('history/clean', () => {
    stepStack.length = 0;
  });
};

export { initHistoryManager };
